/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mukku.hashtables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author acer
 */
public class HashTables { //this week don't focus on collision

    static Scanner kb = new Scanner(System.in);

//    static int N= kb.nextInt();
    static int N = 5;
    static ArrayList<Integer> A = new ArrayList<>(Arrays.asList(new Integer[N]));
    static ArrayList<String> Val = new ArrayList<>(Arrays.asList(new String[N]));
    static int key;
    static String value;

    public HashTables(int N) {
        this.N = N;
        this.key = key;
        this.value = value;
    }

    public int getHash(int key) {
        return key % N;
    }

    public ArrayList put(int key, String value) {
        System.out.println("Put: " + key + ", " + value);
        int index = getHash(key);
        A.remove(index);
        A.add(index, key);
        Val.remove(index);
        Val.add(index, value);
        return A;
    }

    public void get(int key) {
        System.out.println("Get value of key " + key + " : ");
        if (A.contains(key)) {
            int index = getHash(key);
            System.out.println(Val.get(index));
        } else {
            System.out.println("Not Found!!");
        }
    }
//    int now = N;

    public void remove(int key) {
//        now -= 1;
        System.out.println("Remove key " + key + " : ");
        if (A.contains(key)) {
            int index = getHash(key);
            A.remove(index);
            A.add(index, -1);
            Val.remove(index);
            Val.add(index, "");
            printHash();
        } else {
            System.out.println("Not Found!!");
        }
    }

    public static void main(String[] args) {
//        System.out.println("Enter N; N should be Prime Number");
        HashTables h = new HashTables(N);
//        System.out.println(A);
        System.out.println("Input key and value; if number of them less than N you can break with key -1");
        for (int i = 0; i < N; i++) {
            key = kb.nextInt();
            if (key == -1) {
                break;
            }
            value = kb.next();
            h.put(key, value);
//            System.out.println(h.getHash(key));
        }
        printHash();
        h.get(85);
        h.remove(67);
        h.remove(1);
        h.put(51, "X");
        printHash();

        //N=5
        //test: 24 33 1 67 85
        //index: 4 3 1 2 0
        //test2: 24 H 33 M 1 T 67 V 85 B
    }

    public static void printHash() {
        System.out.println("In Hash Table: ");
        for (int i = 0; i < N; i++) {
//            if (A.get(i) != null) {
            System.out.println("index: " + i + " key: " + A.get(i) + " value: " + Val.get(i));
//            }
        }
    }

}
