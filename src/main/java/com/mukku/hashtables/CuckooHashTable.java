/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.hashtables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author acer
 */
public class CuckooHashTable {

    static Scanner kb = new Scanner(System.in);
    static int N = 11;
    static ArrayList<Integer> A = new ArrayList<>(Arrays.asList(new Integer[N]));
    static ArrayList<String> Val = new ArrayList<>(Arrays.asList(new String[N]));
    static ArrayList<Integer> A2 = new ArrayList<>(Arrays.asList(new Integer[N]));
    static ArrayList<String> Val2 = new ArrayList<>(Arrays.asList(new String[N]));
    static int key;
    static String value;

    public CuckooHashTable() {
    }

    public int getHash(int key) {
        return key % N;
    }

    public int getHash2(int key) {
        return (key / N) % N;
    }

    public void put(int key, String value) {
//        System.out.println("Put: " + key + ", " + value);

        inTable1(A, Val, key, value);

//        int index = getHash(key);
//        if (A.get(index) != null) {
//            int index2 = getHash2(A.get(index));
//            if (A2.get(index2) != null) {
//                int index3 = getHash(A2.get(index2));
//                if (A.get(index3) != null) {
//                    int index4 = getHash2(A.get(index3));
//                    if (A2.get(index4) != null) {
//                        int index5 = getHash(A2.get(index4));
//                        if (A.get(index5) != null) {
//                            int index6 = getHash2(A.get(index5));
//                            setIn(A2, Val2, index6, A.get(index5), Val.get(index5));
//                        }
//                        setIn(A, Val, index5, A2.get(index4), Val2.get(index4));
//                    }
//                    setIn(A2, Val2, index4, A.get(index3), Val.get(index3));
//                }
//                setIn(A, Val, index3, A2.get(index2), Val2.get(index2));
//            }
//            setIn(A2, Val2, index2, A.get(index), Val.get(index));
//        }
//        setIn(A, Val, index, key, value);
    }

    public void inTable1(ArrayList A, ArrayList Val, int k, String v) {
//        System.out.println("t1 " + k);
        int index = getHash(k);
        if (A.get(index) == null) {
            setIn(A, Val, index, k, v);
            return;
        }
        int temp1 = (int) A.get(index);
        String temp2 = (String) Val.get(index);
        setIn(A, Val, index, k, v);
        inTable2(A2, Val2, temp1, temp2);
    }

    public void inTable2(ArrayList A2, ArrayList Val2, int k, String v) {
//        System.out.println("t2 " + k);
        int index2 = getHash2(k);
        if (A2.get(index2) == null) {
            setIn(A2, Val2, index2, k, v);
            return;
        }
        int temp1 = (int) A2.get(index2);
        String temp2 = (String) Val2.get(index2);
        setIn(A2, Val2, index2, k, v);
        inTable1(A, Val, temp1, temp2);
    }

    public void setIn(ArrayList A, ArrayList Val, int index, int k, String v) {
        A.set(index, k);
        Val.set(index, v);
    }

    public void get(int key) {
        System.out.println("Get value of key " + key + " : ");
        if (A.contains(key)) {
            int index = getHash(key);
            System.out.println(Val.get(index));
        } else if (A2.contains(key)) {
            int index = getHash2(key);
            System.out.println(Val2.get(index));
        } else {
            System.out.println("Not Found!!");
        }
    }

    public void remove(int key) {
        System.out.println("Remove key " + key + " : ");
        if (A.contains(key)) {
            int index = getHash(key);
            A.set(index, null);
            Val.set(index, null);         
        } else if (A2.contains(key)) {
            int index = getHash2(key);
            A2.set(index, null);
            Val2.set(index, null);         
        } else {
            System.out.println("Not Found!!");
        }
        
        System.out.println("In Hash Table1: ");
            printHash(A, Val);
            System.out.println("In Hash Table2: ");
            printHash(A2, Val2);
    }

    public static void main(String[] args) {
        System.out.println("Enter n : ");
        int n = kb.nextInt();
        CuckooHashTable c = new CuckooHashTable();
        System.out.println("Input key and value :");
        for (int i = 0; i < n; i++) {
            key = kb.nextInt();
            if (key == -1) {
                break;
            }
            value = kb.next();
            c.put(key, value);
//            System.out.println(c.getHash(key));
        }
        System.out.println("In Hash Table1: ");
        printHash(A, Val);
        System.out.println("In Hash Table2: ");
        printHash(A2, Val2);

        c.get(85);
        c.get(100);
        c.get(53);
        c.remove(67);
        c.remove(1);
        c.remove(75);

        //n=8 N=13
        //test:  24 33 1 67 85 28 8 5
        //index: 11  7 1  2  7  2 8 5
        //test2: 24 H 33 M 1 T 67 V 85 B 28 A 8 W 5 X
        /*
        Final hash tables:
        - 1 28 - - 5 - 85 8 - - 24 - 
        - - 33 - - 67 - - - - - - - 
         */
        //N=11;
        /*
        10; without rehash : 20 A 50 B 53 C 75 D 100 E 67 F 105 G 3 H 36 I 39 J
        checkif; pass key 36
        checkrec; pass key 39
        Final hash tables:
        - 100 - 36 - - 50 - - 75 - 
        3 20 - 39 53 - 67 - - 105 - 

        11; have rehash : 20 A 50 B 53 C 75 D 100 E 67 F 105 G 3 H 36 I 39 J 6 K
        105 unpositioned
        REHASH.
        checkrec; pass key 39
        Final hash tables:
        - 67 - 3 - - 39 - - 53 - 
        6 20 - 36 50 - 75 - - 100 - 
         */
    }

    public static void printHash(ArrayList A, ArrayList Val) {
        System.out.print("index: ");
        for (int i = 0; i < N; i++) {
            System.out.print(i + " ");
        }
        System.out.print("\nkey:    ");
        for (int i = 0; i < N; i++) {
            if (A.get(i) != null) {
                System.out.print(A.get(i) + " ");
            } else {
                System.out.print("- ");
            }
        }
        System.out.print("\nvalue: ");
        for (int i = 0; i < N; i++) {
            if (Val.get(i) != null) {
                System.out.print(Val.get(i) + " ");
            } else {
                System.out.print("- ");
            }
        }
        System.out.println("\n------------------");
    }

}
